## **Guides**
***
### **指南**
不论你在建立什么网站，这些指导会尽可能快的帮助你的网站产品化，使用Orchcard团队推荐的最新发布的Orchard Core项目和技术。

#### **入门指南**
大概需要15-30分钟，这些指导为建立任何使用Orchard Core的开发任务建立一个“Hello World”程序，提供了快速、实践后的说明。大多数情况下，只需要了解 .NET SDK和一个编辑器。

* [创建一个模块化的 ASP.NET Core 应用程序](https://orchardcore.readthedocs.io/en/dev/docs/guides/create-modular-application-mvc/)
* [在Startup类中运行代码](https://orchardcore.readthedocs.io/en/dev/docs/guides/run-code-on-startup/)

#### **Orchard Core CMS 指南**
以下指南，面向Orchard Core CMS
* [创建一个 Orchard Core CMS 网站](https://orchardcore.readthedocs.io/en/dev/docs/guides/create-cms-application/)
* [在Admin导航中，创建一个菜单项](https://orchardcore.readthedocs.io/en/dev/docs/guides/)
* [安装本地化文件](https://orchardcore.readthedocs.io/en/dev/docs/guides/install-localization-files/)
* [集成Facebook插件](https://orchardcore.readthedocs.io/en/dev/docs/guides/integrate-facebook-plugins/)

### **教程**
完成需要2-3小时，这些指南提供了深层次的，关于企业应用程序开发课题的上下文探索，让你可以做好实现一个真实的解决方案的准备。
* 从模板中建立一个网站
* 实现一个自助服务的SaaS解决方案。