## **如何在一个模块的Startup中，运行应用程序的任务**

```Startup```类用来初始化服务和部分中间件，在初始化租户的时候调用他们。  
      
接口```OrchardCore.Modules.IModularTenantEvents```提供了当租户第一次被执行时，要执行的用户的方法。   
    
所有的租户都是懒加载的，这意味着处理程序并不会在程序开始的时候就被调用执行，而是在第一次请求它的时候才调用。   
   
下面的例子中```MyStartupTaskService```继承了```ModularTenantEvents```类， 实现了```IModularTenantEvents```接口。
    
```
using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using OrchardCore.Modules;

public class MyStartupTaskService : ModularTenantEvents
{
    private readonly ILogger<MyStartupTaskService> _logger;

    public MyStartupTaskService(ILogger<MyStartupTaskService> logger)
    {
        _logger = logger;
    }

    public override Task ActivatingAsync()
    {
        _logger.LogInformation("A tenant has been activated.");

        return Task.CompletedTask;
    }
}
```

然后在该模块的**Startup.cs**文件中的ConfigureServices()方法中调用下面的方法：
```
services.AddScoped<IModularTenantEvents, MyStartupTaskService>();
```

```ActivatingAsync```会按照注册的顺序，也就是依赖关系的执行。
```ActivatedAsync```执行顺序相反。


当在终端运行程序，你可以看到像下面的结果，在第一次请求被处理之后：
```
info: MyStartupTaskService[0]
      A tenant has been activated.
```